//////////////// Make tools
var toolsSizes = [5, 15, 30, 60];
var toolsCols  = ["white", "red", "green", "blue", "yellow"];
var drawSize = 5;
var drawCol = "green";

var sizeTools = document.getElementById("sizeTools");
for(var i = 0; i < toolsSizes.length; i++) {
    sizeTools.innerHTML +=
        "<button class='sizeButton' onclick='setToolSize("+i+");'><div style='width: "+toolsSizes[i]+"px; height: "+toolsSizes[i]+"px;' id='sizeButton_"+i+"'></div></button><br/>";
}
function setToolSize(i) {
    drawSize = toolsSizes[i];
    for(var j = 0; j < toolsSizes.length; j++)
        document.getElementById("sizeButton_"+j).style.backgroundColor = "white";
    document.getElementById("sizeButton_"+i).style.backgroundColor = "black";
}

var colTools = document.getElementById("colTools");
for(var i = 0; i < toolsCols.length; i++) {
    colTools.innerHTML +=
        "<button class='colButton' id='colButton_"+i+"' onclick='setToolCol("+i+");'  style='background-color: "+toolsCols[i]+"' ></button><br/>";
}
function setToolCol(i) {
    drawCol = toolsCols[i];
    for(var j = 0; j < toolsCols.length; j++)
        document.getElementById("colButton_"+j).style.border = "solid 2px black";
    document.getElementById("colButton_"+i).style.border = "solid 10px black";
}

var sendStartSignal;

async function main() {
    var conns = [];
    var roomList = [];
    var lastAlive = {};
    var scores = {};
    var numWins = {};

    //////////////// Timer
    var timeLeft = 0;
    window.setInterval(function() {
        if(timeLeft > 0) timeLeft--;
        document.getElementById("timeLeft").innerHTML = timeLeft;
        if(timeLeft == 0 && isHost)
            document.getElementById("roundStartDiv").style.visibility = "visible";
        if(isHost)
            conns.forEach(conn => conn.send({numWins: numWins}));
    }, 1000);

    var firstRoundStarted = false;
    sendStartSignal = () => {
        // Calculate winner of last round
        if(firstRoundStarted && isHost) {
            var bestScore = myScore;
            var winner = peer.id;
            for(var i = 0; i < roomList.length; i++) {
                if(scores[roomList[i].id] === null) continue;
                if(scores[roomList[i].id] > bestScore) {
                    bestScore = scores[roomList[i].id];
                    winner = roomList[i].id;
                }
            }
            numWins[winner]++;
            console.log("The winner was "+winner+"!");
            writeScoreboard();
        }
        // Change drawing
        drawingIdx = Math.floor(Math.random()*filenames.length);
        writePerfectImg();
        // Start timers for next round
        var nextTime = document.getElementById("nextTimeInput").value;
        document.getElementById("roundStartDiv").style.visibility = "hidden";
        startRound(nextTime);
        conns.forEach(conn => conn.send({newRound: {nextTime: nextTime, drawingIdx: drawingIdx}}));
        firstRoundStarted = true;
    }
    function startRound(nextTime) {
        timeLeft = nextTime;
        myCtx.fillStyle = "white";
        myCtx.fillRect(0,0,800,500);
        drawTransImage(myCtx);
    }

    //////////////// PeerJS
    async function digestMessage(message) {
        const encoder = new TextEncoder();
        // Append fixed string, to handle the off chance, that someone else is
        // using SHA-256
        const data = encoder.encode(message+"_TEGO_OGET");
        const hashBuffer = await crypto.subtle.digest('SHA-256', data);
        const hashArray = Array.from(new Uint8Array(hashBuffer))
        const digest = hashArray.map(b => b.toString(16).padStart(2, '0')).join('');
        return digest;
    }

    document.getElementById("roomStatus").innerHTML = "Connecting to PeerServer... Try reloading if nothing happens in 10 seconds.";
    // The room name cannot be used as the room ID (i.e. as peer ID of the
    // host), since we are using the public PeerJS PeerServer where collisions
    // could easily occur.
    var roomID = await digestMessage(room);
    var peer = isHost ? new Peer(roomID) : new Peer();
    var host;
    peer.on('open', function(id) {
        console.log("My id is "+id);
        var roomStatus = document.getElementById("roomStatus");
        if(isHost) {
            var inviteURL = window.location.href+"#/"+room;
            roomStatus.innerHTML =
                "You are hosting this room. Invite friends to your room by giving them the link <a target='_blank' href='"+inviteURL+"'>"+inviteURL+"</a>";
            document.getElementById("gameArea").style.display = "block";
            document.getElementById("roundStartDiv").style.visibility = "visible";
            window.setInterval(function(){
                var t = (new Date()).getTime();
                for(var i = 1; i < roomList.length; i++) {
                    if(t-lastAlive[roomList[i].id] > 10000) {
                        roomList.splice(i, 1);
                        conns.forEach(c => c.send({roomList: roomList}));
                        writeRoomList();
                    }
                }
            }, 5000);
        }
        else {
            roomStatus.innerHTML =
                "Connecting to room... Reload if nothing happens in 10 seconds.";
            host = peer.connect(roomID);
            host.on('open', function(data) {
                conns.push(host);
                roomStatus.style.display = "none";
                document.getElementById("gameArea").style.display = "block";
                host.send({newBuddy: {id: peer.id, name: myName}});
                writeRoomList();
            });
            window.setInterval(function(){
                host.send({alive: peer.id});
            }, 1000);
        }

        // Connect to everyone in roomList
        var doneConnecting = true;
        window.setInterval(function() {
            if(!doneConnecting) return;
            doneConnecting = false;
            // Remove conn if not in roomList
            for(var i = 0; i < conns.length; i++) {
                var inRoomList = false;
                for(var j = 0; j < roomList.length; j++)
                    if(roomList[j].id === conns[i].peer)
                        inRoomList = true;
                if(!inRoomList)
                    conns.splice(i, 1);
            }
            // Add conn if new from roomList
            for(var i = 0; i < roomList.length; i++) {
                var newBuddy = true;
                for(var j = 0; j < conns.length; j++)
                    if(conns[j].peer === roomList[i].id)
                        newBuddy = false;
                if(newBuddy && (roomList[i].id !== peer.id)) {
                    console.log("Need to connect to "+roomList[i].id);
                    var conn = peer.connect(roomList[i].id);
                    conn.on('open', function(data) {
                        conns.push(conn);
                    });
                }
            }
            doneConnecting = true;
        }, 5000);

        // Distribute canvas image
        var doneSending = true;
        window.setInterval(function() {
            if(!doneSending) return;
            doneSending = false;
            sendImage();
            doneSending = true;
        }, 1000);

        var doneScoring = true;
        window.setInterval(function() {
            if(!doneSending) return;
            doneScoring = false;
            myScore = getScore();
            document.getElementById("myScore").innerHTML = myScore;
            doneScoring = true;
        }, 250);
    });

    peer.on('connection', function(conn) {
        conn.on('data', function(data) {
            //console.log(data);
            if("imageURL" in data) {
                document.getElementById("drawing_"+data.id).src = data.imageURL;
                document.getElementById("score_"+data.id).innerHTML = data.score;
                scores[data.id] = data.score;
            }
            if("newBuddy" in data) {
                if(isHost) { // should always be true if "newBuddy" in data, but whatever
                    if(roomList.length === 0) {
                        roomList.push({id: peer.id, name: myName});
                        numWins[peer.id] = 0;
                    }
                    roomList.push(data.newBuddy);
                    numWins[data.newBuddy.id] = 0;
                    lastAlive[data.newBuddy.id] = (new Date()).getTime();
                    var newConn = peer.connect(data.newBuddy.id);
                    newConn.on('open', function(data) {
                        conns.push(newConn);
                        conns.forEach(c => c.send({roomList: roomList}));
                    });
                    writeRoomList();
                }
            }
            if("roomList" in data) {
                roomList = data.roomList;
                writeRoomList();
            }
            if("alive" in data) {
                lastAlive[data.alive] = (new Date()).getTime();
            }
            if("newRound" in data) {
                drawingIdx = data.newRound.drawingIdx;
                writePerfectImg();
                startRound(data.newRound.nextTime);
            }
            if("numWins" in data) {
                numWins = data.numWins;
                writeScoreboard();
            }
        });
    });

    function writeRoomList() {
        writeScoreboard();
        var competitorsEl = document.getElementById("competitors");
        competitorsEl.innerHTML = "";
        roomList.forEach(x => {
            if(x.id != peer.id)
                competitorsEl.innerHTML += "<div class='buddyDrawingDiv'><b>"+x.name+"</b> has coloured <span id='score_"+x.id+"'>0</span>% correctly:<br/>\n<img class='buddyDrawing' id='drawing_"+x.id+"'></img></div>\n";
        });
    }

    function writeScoreboard() {
        var scoreboardEl = document.getElementById("scoreboard");
        scoreboardEl.innerHTML = "<tr><th>Name</th><th>Rounds won</th></tr>\n";
        console.log(roomList)
        roomList.sort(
            (a, b) => {
                if(numWins[a.id] === numWins[b.id])
                    return a.name > b.name ? 1 : -1;
                else
                    return numWins[a.id] < numWins[b.id] ? 1 : -1;
        }).forEach(x => {
            scoreboardEl.innerHTML +=
                "<tr><td>"+x.name+"</td><td>"+numWins[x.id]+"</td></tr>\n";
        });
    }

    function sendImage() {
        drawingImageURL = myCanvas.toDataURL();
        conns.forEach(conn => conn.send({id: peer.id, name: myName, imageURL: drawingImageURL, score: myScore}));
    }


    //////////////// Canvas drawing
    var drawingIdx = Math.floor(Math.random()*filenames.length);
    var perfectCanvas = document.createElement("canvas");
    perfectCanvas.width = 800;
    perfectCanvas.height = 500;
    var perfectCtx = perfectCanvas.getContext("2d");
    var perfectImg = document.getElementById("perfectImg");
    perfectImg.onload = function() {perfectCtx.drawImage(perfectImg, 0, 0);};
    function writePerfectImg() {
        perfectImg.src = 'drawings/'+filenames[drawingIdx]+'.png';
    }
    writePerfectImg();
    function drawTransImage(ctx) {
        var transImg = new Image();
        transImg.onload = function() {ctx.drawImage(transImg, 0, 0);};
        transImg.src = 'drawings/'+filenames[drawingIdx]+'-trans.png';
    }

    var myScore = 0;
    function getScore() {
        var equal = 0;
        var my = myCtx.getImageData(0, 0, 800, 500).data;
        var perfect = perfectCtx.getImageData(0, 0, 800, 500).data;
        for(var i = 0; i < my.length; i += 4) {
            if(Math.abs(my[i]-perfect[i]) <= 1 && Math.abs(my[i+1]-perfect[i+1]) <= 1 && Math.abs(my[i+2]-perfect[i+2]) <= 1)
                equal++;
        }
        return 100*equal/(500*800);
    }

    var myCanvas;
    var myCtx;
    function initCanvas() {
        myCanvas = document.getElementById("myCanvas");
        myCtx = myCanvas.getContext("2d");
        var ctx = myCtx;
        ctx.fillStyle = "white";
        ctx.beginPath();
        ctx.rect(0,0,800,500);
        ctx.closePath();
        ctx.fill();
        drawTransImage(ctx);

        document.addEventListener("mousedown",
                                  (e) => drawStart(e, ctx, myCanvas),
                                  false);
        document.addEventListener("mouseup",
                                  (e) => drawEnd(e, ctx, myCanvas),
                                  false);
        document.addEventListener("mousemove",
                                  (e) => drawMove(e, ctx, myCanvas),
                                  false);
        myCanvas.addEventListener("touchstart",
                                  (e) => drawStart(e.touches[0], ctx, myCanvas),
                                  false);
        myCanvas.addEventListener("touchend",
                                  (e) => drawEnd(e.changedTouches[0], ctx, myCanvas),
                                  false);
        myCanvas.addEventListener("touchmove",
                                  (e) => {drawMove(e.touches[0], ctx, myCanvas); e.preventDefault()},
                                  false);
    }

    var x, y;
    var mouseIsDown = false;
    var drawingImageURL;
    function drawStart(e, ctx, canvas) {
        if(timeLeft <= 0) return;
        mouseIsDown = true;
        x = e.pageX - canvas.offsetLeft;
        y = e.pageY - canvas.offsetTop;
        ctx.beginPath();
        ctx.fillStyle = drawCol;
        ctx.strokeStyle = drawCol;
        ctx.arc(x, y, drawSize/2, 0, 2 * Math.PI);
        ctx.fill();
        drawTransImage(ctx);
        //sendImage();
    }
    function drawEnd(e, ctx, canvas) {
        mouseIsDown = false;
    }
    function drawMove(e, ctx, canvas) {
        if(timeLeft <= 0 || !mouseIsDown) return;
        var prevX = x;
        var prevY = y;
        x = e.pageX - canvas.offsetLeft;
        y = e.pageY - canvas.offsetTop;
        ctx.beginPath();
        ctx.fillStyle = drawCol;
        ctx.strokeStyle = drawCol;
        ctx.moveTo(prevX, prevY);
        ctx.lineTo(x, y);
        ctx.lineWidth = drawSize;
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.arc(x, y, drawSize/2, 0, 2 * Math.PI);
        ctx.fill();
        drawTransImage(ctx);
        //sendImage();
    }

    initCanvas();
}

//////////////// Choose room name
var url = new URL(window.location.href);
var room = url.hash.slice(2);
var isHost = !room;
if(isHost) {
    document.getElementById("roomNameSelector").style.display = "block";
} else {
    document.getElementById("usernameSelector").style.display = "block";
}

function selectRoomName() {
    var suggestion = document.getElementById("roomName").value;
    if(/^[a-zA-Z0-9_\-]{5,}$/.test(suggestion)) {
        room = suggestion;
        document.getElementById("roomNameSelector").style.display = "none";
        document.getElementById("usernameSelector").style.display = "block";
    } else {
        alert("Room name must be at least 5 characters long and contain only letters, numbers, dashes and underscores.");
    }
}
document.getElementById("roomNameForm").addEventListener('submit', e => {
    e.preventDefault();
    selectRoomName();
});

//////////////// Choose username
myName = null;

function selectUsername() {
    var suggestion = document.getElementById("username").value;
    if(suggestion !== null && 2 <= suggestion.length && suggestion.length <= 30) {
        myName = suggestion;
        document.getElementById("usernameSelector").style.display = "none";
        main();
    } else {
        alert("Username must be between 2 and 30 characters.");
    }
}
document.getElementById("usernameForm").addEventListener('submit', e => {
    e.preventDefault();
    selectUsername();
});
